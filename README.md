# Checksum Calculator

## Type of checksum supported

- checksum8 xor
- checksum8 modulo 256
- checksum8 2s complement

## Issues/Bug report or improvement ideas
https://gitlab.com/olive007/case-convert/-/issues

## License
GNU Lesser General Public License v3 or later (LGPLv3+)
