import unittest

from checksum_calculator import *


class TestStringMethods(unittest.TestCase):

    def test_compute_checksum(self):
        self.assertEqual("96", compute_checksum("10018200534E203A2047572F32312F3030312020", ChecksumType.XOR))
        self.assertEqual("00", compute_checksum("10018200534E203A2047572F32312F303031202096", ChecksumType.XOR))
        self.assertEqual("DE", compute_checksum("10018200534E203A2047572F32312F3030312020", ChecksumType.MOD256))
        self.assertEqual("22", compute_checksum("10018200534E203A2047572F32312F3030312020", ChecksumType.COMP_2S))

    def test_compute_checksum8_xor(self):
        self.assertEqual("96", compute_checksum8_xor("10018200534E203A2047572F32312F3030312020"))
        self.assertEqual("00", compute_checksum8_xor("10018200534E203A2047572F32312F303031202096"))
        self.assertEqual("28", compute_checksum8_xor("10012E005452414954454D454E5420454E205041"))
        self.assertEqual("00", compute_checksum8_xor("10012E005452414954454D454E5420454E20504128"))

        self.assertEqual("96", compute_checksum8_xor("10018200534e203a2047572f32312f3030312020"))
        self.assertEqual("00", compute_checksum8_xor("10018200534e203a2047572f32312f303031202096"))
        self.assertEqual("28", compute_checksum8_xor("10012e005452414954454d454e5420454e205041"))
        self.assertEqual("00", compute_checksum8_xor("10012e005452414954454d454e5420454e20504128"))

    def test_compute_checksum8_mod256(self):
        self.assertEqual("DE", compute_checksum8_mod256("10018200534E203A2047572F32312F3030312020"))
        self.assertEqual("A0", compute_checksum8_mod256("10012E005452414954454D454E5420454E205041"))

        self.assertEqual("DE", compute_checksum8_mod256("10018200534e203a2047572f32312f3030312020"))
        self.assertEqual("A0", compute_checksum8_mod256("10012e005452414954454d454e5420454e205041"))

    def test_compute_checksum8_2s_complement(self):
        self.assertEqual("22", compute_checksum8_2s_complement("10018200534E203A2047572F32312F3030312020"))
        self.assertEqual("60", compute_checksum8_2s_complement("10012E005452414954454D454E5420454E205041"))

        self.assertEqual("22", compute_checksum8_2s_complement("10018200534e203a2047572f32312f3030312020"))
        self.assertEqual("60", compute_checksum8_2s_complement("10012e005452414954454d454e5420454e205041"))

    def test_is_checksum8_xor(self):
        self.assertTrue(is_checksum8_xor("10018200534E203A2047572F32312F303031202096"))
        self.assertTrue(is_checksum8_xor("10012E005452414954454D454E5420454E20504128"))

        self.assertTrue(is_checksum8_xor("10018200534e203a2047572f32312f303031202096"))
        self.assertTrue(is_checksum8_xor("10012e005452414954454d454e5420454e20504128"))

    def test_is_checksum8_mod256(self):
        self.assertTrue(is_checksum8_mod256("10018200534E203A2047572F32312F3030312020DE"))
        self.assertTrue(is_checksum8_mod256("10012E005452414954454D454E5420454E205041A0"))

        self.assertTrue(is_checksum8_mod256("10018200534e203a2047572f32312f3030312020de"))
        self.assertTrue(is_checksum8_mod256("10012e005452414954454d454e5420454e205041a0"))

    def test_is_checksum8_2s_complement(self):
        self.assertTrue(is_checksum8_2s_complement("10018200534E203A2047572F32312F303031202022"))
        self.assertTrue(is_checksum8_2s_complement("10012E005452414954454D454E5420454E20504160"))

        self.assertTrue(is_checksum8_2s_complement("10018200534e203a2047572f32312f303031202022"))
        self.assertTrue(is_checksum8_2s_complement("10012e005452414954454d454e5420454e20504160"))
